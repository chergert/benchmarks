#include <gtk/gtk.h>

static const char *color_hex[] = {
  "#ffbe6f", "#ffa348", "#ff7800", "#e66100", "#c64600",
  "#99c1f1", "#62a0ea", "#3584e4", "#1c71d8", "#1a5fb4",
  "#8ff0a4", "#57e389", "#33d17a", "#2ec27e", "#26a269",
  "#dc8add", "#c061cb", "#9141ac", "#813d9c", "#613583",
  "#f9f06b", "#f8e45c", "#f6d32d", "#f5c211", "#e5a50a",
  "#cdab8f", "#b5835a", "#986a44", "#865e3c", "#63452c",
};
static GdkRGBA colors[G_N_ELEMENTS(color_hex)];
static int TILE_WIDTH = 128;
static int TILE_HEIGHT = 128;

static inline void
premix_colors (GdkRGBA       *dest,
               const GdkRGBA *fg,
               const GdkRGBA *bg,
               gboolean       bg_set,
               double         alpha)
{
  g_assert (dest != NULL);
  g_assert (fg != NULL);
  g_assert (bg != NULL || bg_set == FALSE);
  g_assert (alpha >= 0.0 && alpha <= 1.0);

  if (bg_set)
    {
      dest->red = ((1 - alpha) * bg->red) + (alpha * fg->red);
      dest->green = ((1 - alpha) * bg->green) + (alpha * fg->green);
      dest->blue = ((1 - alpha) * bg->blue) + (alpha * fg->blue);
      dest->alpha = 1.0;
    }
  else
    {
      *dest = *fg;
      dest->alpha = alpha;
    }
}

struct _MyGrid
{
  GtkWidget parent;
  GArray *items;
};

typedef struct _GridItem
{
  graphene_rect_t alloc;
  GdkRGBA from;
  GdkRGBA to;
} GridItem;

G_DECLARE_FINAL_TYPE (MyGrid, my_grid, MY, GRID, GtkWidget)
G_DEFINE_FINAL_TYPE (MyGrid, my_grid, GTK_TYPE_WIDGET)

static void
my_grid_finalize (GObject *object)
{
  MyGrid *self = MY_GRID (object);

  g_clear_pointer (&self->items, g_array_unref);

  G_OBJECT_CLASS (my_grid_parent_class)->finalize (object);
}

static void
my_grid_snapshot (GtkWidget   *widget,
                  GtkSnapshot *snapshot)
{
  MyGrid *self = MY_GRID (widget);
  double alpha = (g_get_monotonic_time () % (G_USEC_PER_SEC * 3)) / (double)(G_USEC_PER_SEC*1.5);

  if (alpha > 1)
    alpha = 2. - alpha;

  alpha = CLAMP (alpha, .0, 1.);

  for (guint i = 0; i < self->items->len; i++)
    {
      const GridItem *item = &g_array_index (self->items, GridItem, i);
      GdkRGBA color;

      premix_colors (&color, &item->from, &item->to, TRUE, alpha);

      gtk_snapshot_append_color (snapshot, &color, &item->alloc);
    }
}

static void
my_grid_size_allocate (GtkWidget *widget,
                       int        width,
                       int        height,
                       int        baseline)
{
  MyGrid *self = MY_GRID (widget);

  if (self->items->len > 0)
    g_array_remove_range (self->items, 0, self->items->len);

  GTK_WIDGET_CLASS (my_grid_parent_class)->size_allocate (widget, width, height, baseline);

  for (guint y = 0; y < height; y += TILE_HEIGHT)
    {
      for (guint x = 0; x < width; x += TILE_WIDTH)
        {
          GridItem item = {0};

          item.alloc.origin.x = x;
          item.alloc.origin.y = y;
          item.alloc.size.width = TILE_WIDTH;
          item.alloc.size.height = TILE_HEIGHT;

          item.from = colors[g_random_int_range (0, G_N_ELEMENTS (colors))];
          item.to = colors[g_random_int_range (0, G_N_ELEMENTS (colors))];

          g_array_append_val (self->items, item);
        }
    }
}

static void
my_grid_class_init (MyGridClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = my_grid_finalize;

  widget_class->snapshot = my_grid_snapshot;
  widget_class->size_allocate = my_grid_size_allocate;
}

static void
my_grid_init (MyGrid *self)
{
  self->items = g_array_new (FALSE, FALSE, sizeof (GridItem));
}

static gboolean
tick_cb (GtkWidget     *widget,
         GdkFrameClock *frame_clock,
         gpointer       user_data)
{
  gtk_widget_queue_draw (widget);
  return G_SOURCE_CONTINUE;
}

int
main (int       argc,
      char *argv[])
{
  GOptionContext *context;
  GtkWindow *window;
  GMainLoop *main_loop;
  MyGrid *grid;
  GError *error = NULL;
  GOptionEntry entries[] = {
    { "width", 'w', 0, G_OPTION_ARG_INT, &TILE_WIDTH, "the tile width" },
    { "height", 'h', 0, G_OPTION_ARG_INT, &TILE_HEIGHT, "the tile height" },
    { 0 }
  };

  context = g_option_context_new ("");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    g_error ("%s", error->message);

  gtk_init ();

  for (guint i = 0; i < G_N_ELEMENTS (color_hex); i++)
    gdk_rgba_parse (&colors[i], color_hex[i]);

  main_loop = g_main_loop_new (NULL, FALSE);

  window = g_object_new (GTK_TYPE_WINDOW, NULL);
  grid = g_object_new (my_grid_get_type (), NULL);
  gtk_window_set_child (window, GTK_WIDGET (grid));

  gtk_widget_add_tick_callback (GTK_WIDGET (grid), tick_cb, NULL, NULL);

  gtk_window_fullscreen (window);
  gtk_window_present (window);

  g_main_loop_run (main_loop);
}
