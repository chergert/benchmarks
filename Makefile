all: grid

WARNINGS := -Wall
OPTS := -O2
DEBUG := -ggdb
PKGS := gtk4

grid: grid.c
	$(CC) -o $@.tmp $(WARNINGS) $(OPTS) $(DEBUG) $(shell pkg-config --cflags --libs $(PKGS)) $<
	mv $@.tmp $@

clean:
	rm -rf *.tmp grid
